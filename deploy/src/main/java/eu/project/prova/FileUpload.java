package eu.project.prova;
//package specs.sws_application.frontend.struts;
  
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import eu.specs.datamodel.enforcement.ImplementationPlan;
  
@WebServlet("/FileUpload")

/* fileSizeThreshold : La dimensione del file in byte dopo il quale il file 
					   verr� memorizzato temporaneamente sul disco . 
                       La dimensione predefinita � 0 byte .
   MaxFileSize : La dimensione massima consentita per i file caricati , in byte . 
                 Se la dimensione di un file caricato � pi� grande di questa dimensione, 
                 il web container dar� un'eccezione ( IllegalStateException ) . 
                 La dimensione predefinita � illimitata
   maxRequestSize : La dimensione massima consentita per una richiesta di 
                    multipart / form-data , in byte . 
                    Il contenitore web sar� un'eccezione se la dimensione 
                    complessiva di tutti i file caricati supera questa soglia . 
                    La dimensione predefinita � illimitata .
*/
@MultipartConfig(fileSizeThreshold=1024*1024*10,    // 10 MB 
                 maxFileSize=1024*1024*50,          // 50 MB
                 maxRequestSize=1024*1024*100)      // 100 MB
public class FileUpload extends HttpServlet {
  
    private static final long serialVersionUID = 205242440643911308L;
    String WebServerIp="";
    final static String path="manage_app/";
    
    //gestisce richiesta post proveniente dalla form http
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
    		WebServerIp="////"+ListFileAction.haProxyIp+"//";
    		String idsla=request.getParameter("idsla");
        	String fileName = null;
        	Part part=request.getPart("fileName");
            //filename nome pi� estensione del file caricato
        	fileName = getFileName(part);
        	if((fileName.trim().endsWith("war"))||(fileName.trim().endsWith("zip"))||fileName.trim().endsWith("tar.gz")){
        		response.setStatus(307);
        		response.setHeader("Location", this.WebServerIp+this.path);
        		//System.out.println(this.WebServerIp+this.path);
        		}
        	else{
        		response.setStatus(302);
        		request.setAttribute("ERROR", "File not valid");
        		//request.setAttribute("idsla", idsla);
        		String risposta = request.getHeader("Referer");
        		//System.out.println(risposta);
        		//response.setHeader("Location", risposta.replaceAll("&ERROR=File%20not%20valid", "")+"&ERROR=File not valid");
        		response.setHeader("Location", risposta);
            }
    		
    }
      
    /**
     * Utility method to get file name from HTTP header content-disposition
     */
    private String getFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        //System.out.println("content-disposition header= "+contentDisp);
        String[] tokens = contentDisp.split(";");
        for (String token : tokens) {
            if (token.trim().startsWith("filename")) {
                return token.substring(token.indexOf("=") + 2, token.length()-1);
            }
        }
        return "";
    }
}