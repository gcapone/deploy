package eu.project.prova;
//package specs.sws_application.frontend.struts;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import eu.specs.datamodel.enforcement.ImplementationPlan;
import static java.nio.file.StandardCopyOption.*;

public class ListFileAction extends Action{

	final static String path="manage_app/";
	static List<String> listaIP = new ArrayList<String>();
	static String haProxyIp;
	List<String> listaallfile = new ArrayList<String>();
	List<List<String>> listaf = new ArrayList<List<String>>();
	int num=0;
	int max=0;

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
					throws Exception {

		List<String> status = new ArrayList<String>();
		List<String> data = new ArrayList<String>();
		String ris="";
		System.out.println("ricevuto richeista");
		for (int i=0;i<listaIP.size();i++){
			System.out.println("ip: "+listaIP.get(i));
		}

		if(request.getParameter("controlla")!=null){
			System.out.println("chiesto di controllare "+request.getParameter("controlla"));
			ris=verifica(listaf,request.getParameter("controlla"),max,num,listaIP);
			request.setAttribute("singolostato", ris);
		}
		else if(request.getParameter("cancella")!=null){
			System.out.println("chiesto di cancellare "+request.getParameter("cancella"));
			ris=cancella(listaf,request.getParameter("cancella"),num,listaIP,max);
		}
		else if(request.getParameter("stop")!=null){
			System.out.println("chiesto di stoppare "+request.getParameter("stop"));
			stopAll(request.getParameter("stop"),num,listaIP,max,listaf);
		}
		else if(request.getParameter("start")!=null){
			System.out.println("chiesto di avviare "+request.getParameter("start"));
			start(listaf,request.getParameter("start"),num,listaIP,max);
		}
		else if(request.getParameter("sync")!=null){
			System.out.println("chiesto di sincronizzare "+request.getParameter("sync"));
			sync(request.getParameter("sync"),listaIP,listaf,max,num);
			deploy(listaIP,request.getParameter("sync"));
		}
//		else if(request.getParameter("json")!=null){
		else {
			System.out.println("json non nullo");
			String line;
			ImplementationPlan myplan;
			String slaid="";
			listaIP.clear();
			listaallfile.clear();
			listaf.clear();
			String slaId = request.getParameter("json");
			try {
				Client client = Client.create();

				WebResource webResource = client
						.resource("http://apps.specs-project.eu/implementation-api/impl-plans/"+slaId);

				ClientResponse response2 = webResource.accept("application/json")
						.get(ClientResponse.class);

				if (response.getStatus() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
							+ response.getStatus());
				}

				String output = response2.getEntity(String.class);

				System.out.println("Output from Server .... \n"+output);

				ObjectMapper mapper = new ObjectMapper();
				myplan = mapper.readValue(output, ImplementationPlan.class);

				slaid=myplan.getId();
				for(int i=0;i<myplan.getPools().get(0).getVms().size();i++){
					for(int j=0;j<myplan.getPools().get(0).getVms().get(i).getComponents().size();j++){
						if(!myplan.getPools().get(0).getVms().get(i).getComponents().get(j).getRecipe().equals("haproxy")){
							listaIP.add(myplan.getPools().get(0).getVms().get(i).getPublicIp());
						}
						else{
							haProxyIp=myplan.getPools().get(0).getVms().get(i).getPublicIp();
						}
					}
				}
				num=listaIP.size();
				listaallfile=sendPOST(num,listaIP);
				stampa(listaallfile);
				leggiJson(listaallfile,listaf,num);
				max=max(listaf);

				verifica(listaf,status,data,max,num,listaIP);

				//Variabili da inviare alla pagina jsp
				request.setAttribute("listafile", listaf.get(max));
				request.setAttribute("stato", status);
				request.setAttribute("tempo", data);

				// Prendo l'id dello sla selezionato
				request.setAttribute("idsla", slaid);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return new ActionForward(mapping.getForward());
	}

	public List<String> getIP(){return listaIP;}

	private void verifica(List<List<String>> dacontrollare, List<String> controllo,List<String> date,int max,int n,List<String>IP){
		int i=0;
		String verify="";
		while(i<dacontrollare.get(max).size() ){
			int cont1=0;
			int cont2=0;
			for(int j=0;j<n;j++){
				if(dacontrollare.get(j).contains(dacontrollare.get(max).get(i))==true){
					cont1++;
					//possibile controllo se web app � running and sync or not
					String app=dacontrollare.get(max).get(i);
					String[] parts = app.split("\\.");
					String request2 = "http://"+IP.get(j)+"/"+this.path+parts[0];
					//System.out.println(request2);
					try{
						URL    url2 = new URL( request2 );
						HttpURLConnection conn2= (HttpURLConnection) url2.openConnection();           
						conn2.setDoOutput( true );
						conn2.setRequestMethod( "GET" );
						conn2.setRequestProperty( "charset", "utf-8");
						conn2.setUseCaches( false );
						int responseCode = conn2.getResponseCode();
						if (responseCode>=200 && responseCode<300){
							cont2++;
						}
					}catch(Exception e){e.printStackTrace();}
				}
			}//end for
			if(cont1==n && cont2!=n) verify="deployed and sync";
			else if(cont1==n && cont2==n) verify="running and sync";
			else if(cont1!=n && cont2!=0) verify="running and not sync";
			else verify="deployed and not sync";
			i++;
			date.add(getData());
			controllo.add(verify);
		}//end while
	}

	private String verifica(List<List<String>> listacompleta, String dacontrollare,int max,int n,List<String>IP){
		String ris="";
		String verify="";
		int cont1=0;
		int cont2=0;
		for(int j=0;j<n;j++){
			if(listacompleta.get(j).contains(dacontrollare)==true){
				cont1++;
				//possibile controllo se web app � running and sync or not
				String app=dacontrollare;
				String[] parts = app.split("\\.");
				String request2 = "http://"+IP.get(j)+"/"+this.path+parts[0];
				System.out.println("verifica - request2 vale: "+request2);
				try{
					URL    url2 = new URL( request2 );
					HttpURLConnection conn2= (HttpURLConnection) url2.openConnection();           
					conn2.setDoOutput( true );
					conn2.setRequestMethod( "GET" );
					conn2.setRequestProperty( "charset", "utf-8");
					conn2.setUseCaches( false );
					int responseCode = conn2.getResponseCode();
					if (responseCode>=200 && responseCode<300){
						cont2++;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			else{
				System.out.println("verifica - sono nell'else");
			}
		}
		if(cont1==n && cont2!=n) 
			verify="deployed and sync";
		else if(cont1==n && cont2==n) 
			verify="running and sync";
		else if(cont1!=n && cont2!=0) 
			verify="running and not sync";
		else 
			verify="deployed and not sync";

		ris=verify+";"+getData();
		return ris;
	}

	private void stopAll(String dastoppare,int n,List<String> IP,int max,List<List<String>>listaf){
		String ris="";
		Boolean trovato=false;
		int i=0;
		ris=verifica(listaf,dastoppare,max,n,IP);
		String[] parts=ris.split(";");
		if((parts[0].equals("running and not sync")) || (parts[0].equals("running and sync"))){
			while(trovato==false){
				String app=dastoppare;
				String[] parts1 = app.split("\\.");
				String request2 = "http://"+IP.get(i)+"/"+this.path+parts1[0];
				i++;
				try{
					URL    url2 = new URL( request2 );
					HttpURLConnection conn2= (HttpURLConnection) url2.openConnection();           
					conn2.setDoOutput( true );
					conn2.setRequestMethod( "GET" );
					conn2.setRequestProperty( "charset", "utf-8");
					conn2.setUseCaches( false );
					int responseCode = conn2.getResponseCode();
					if (responseCode>=200 && responseCode<300){
						trovato=true;
					}
				}catch(Exception e){e.printStackTrace();}
			}
		}
		stop(dastoppare,IP,i-1);
		sync (dastoppare,IP,listaf,max,n);
	}

	private String stop(String dastoppare,List<String> IP,int index){
		String verify="error";
		String app=dastoppare;
		String[] parts = app.split("\\.");
		String request3 = "http://"+IP.get(index)+"/"+this.path+"?stop="+dastoppare;
		try{
			URL    url3 = new URL( request3 );
			HttpURLConnection conn3= (HttpURLConnection) url3.openConnection();           
			conn3.setDoOutput( true );
			conn3.setRequestMethod( "GET" );
			conn3.setRequestProperty( "charset", "utf-8");
			conn3.setUseCaches( false );
			int responseCode2 = conn3.getResponseCode();
			if (responseCode2>=200 && responseCode2<300){
				verify="ok";
			}
		}catch(Exception e){e.printStackTrace();}
		return verify;
	}

	private void start(List<List<String>> l,String daavviare,int n,List<String> IP,int max){
		String ris="";
		ris=verifica(l,daavviare,max,n,IP);
		String[] parts=ris.split(";");
		if(parts[0].equals("running and not sync")){
			stopAll(daavviare,n,IP,max,l);
			for(int i=0;i<n;i++)
				try{
					deploy(IP,daavviare);
				}catch(Exception e){}
			startAll(daavviare,n,IP);	
		}
		else if(parts[0].equals("deployed and sync")){
			startAll(daavviare,n,IP);
		}
		else if(parts[0].equals("deployed and not sync")){
			for(int i=0;i<n;i++)
				try{
					deploy(IP,daavviare);
				}catch(Exception e){}
			startAll(daavviare,n,IP);
		}
	}

	private void startAll(String daavviare,int n,List<String> IP){
		String verify="";
		for(int i=0;i<n;i++){
			String request3 = "http://"+IP.get(i)+"/"+this.path+"?start="+daavviare;
			try{
				URL    url3 = new URL( request3 );
				HttpURLConnection conn3= (HttpURLConnection) url3.openConnection();           
				conn3.setDoOutput( true );
				conn3.setRequestMethod( "GET" );
				conn3.setRequestProperty( "charset", "utf-8");
				conn3.setUseCaches( false );
				int responseCode2 = conn3.getResponseCode();
				if (responseCode2>=200 && responseCode2<300){
					verify="ok";
				}
			}catch(Exception e){e.printStackTrace();}
		}}

	private String cancella(List<List<String>> l,String dacancellare, int n, List<String> IP,int max){
		String status="error";
		String ris=verifica(l,dacancellare,max,n,IP);
		String[] parts=ris.split(";");
		if((parts[0].equals("running and not sync")) || (parts[0].equals("running and sync"))){
			stopAll(dacancellare,n,IP,max,l);
		}
		for(int i=0;i<n;i++){
			if(l.get(i).contains(dacancellare)){
				String request2 = "http://"+IP.get(i)+"/"+this.path+"?cancella="+dacancellare;
				//System.out.println(request2);
				try{
					URL    url2 = new URL( request2 );
					HttpURLConnection conn2= (HttpURLConnection) url2.openConnection();           
					conn2.setDoOutput( true );
					conn2.setRequestMethod( "GET" );
					conn2.setRequestProperty( "charset", "utf-8");
					conn2.setUseCaches( false );
					int responseCode = conn2.getResponseCode();
					if (responseCode>=200 && responseCode<300){
						status="deleted";
					}
				}catch(Exception e){e.printStackTrace();}
			}}
		return status;
	}

	private void sync (String appNameToBeSync, List<String> ipList,List<List<String>> listaf, int max, int numberOfLists){
		String app_status=verifica(listaf,appNameToBeSync,max,numberOfLists,ipList);
		System.out.println("sync - app_status 1 : "+app_status);
		String[] parts=app_status.split(";");
		//System.out.println(parts[0]);
		if(parts[0].equals("running and not sync")){
			stopAll(appNameToBeSync,numberOfLists,ipList,max,listaf);
			app_status=verifica(listaf,appNameToBeSync,max,numberOfLists,ipList);
			System.out.println("sync - app_status 2 : "+app_status);
			String[] parts1=app_status.split(";");
			if(parts1[0].equals("deployed and not sync")){
				for(int i=0;i<numberOfLists;i++)
					try{
						deploy(ipList,appNameToBeSync);
					}catch(Exception e){}
			}
		}
		else if(parts[0].equals("deployed and not sync")) {
			for(int i=0;i<numberOfLists;i++)
				try{
					deploy(ipList,appNameToBeSync);
				}catch(Exception e){}
		}
	}

	private String getData(){

		Calendar c = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");	
		String data=sdf.format(c.getTime());
		return data;
	}		

	private List<String> sendPOST(int dim,List<String> IP){
		System.out.println("sendPOST - "+IP.size());
		for(int i = 0;i<IP.size();i++){
			System.out.println("ip: "+IP.get(i));
		}

		List<String> listaallfile = new ArrayList<String>();
		for(int i=0;i<dim;i++){
			String risposta="error";
			String WebServer = "http://"+IP.get(i)+"/";
			//System.out.println("invio Post numero "+i+" al sever "+WebServer);
			//invio Post con parametro richiesta settato a listafile
			String urlParameters  = "richiesta=listafile";
			byte[] postData       = urlParameters.getBytes( StandardCharsets.UTF_8 );
			int    postDataLength = postData.length;
			String request1        = WebServer+this.path;
			try{
				URL    url            = new URL( request1 );
				HttpURLConnection conn= (HttpURLConnection) url.openConnection();           
				conn.setDoOutput( true );
				conn.setRequestMethod( "POST" );
				conn.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded"); 
				conn.setRequestProperty( "charset", "utf-8");
				conn.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));
				conn.setUseCaches( false );
				try( DataOutputStream wr = new DataOutputStream( conn.getOutputStream())) {
					wr.write( postData );
				}catch (Exception ex){ex.printStackTrace();}

				//controllo lo stato della risposta
				int responseCode = conn.getResponseCode();
				if(responseCode==200){
					System.out.println("risposta 200ok");
					try{
						StringBuilder str=new StringBuilder();
						BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
						String line = in.readLine();
						while(line!=null) {
							//System.out.println(line);
							str.append(line);
							line = in.readLine();
						}
						risposta=str.toString();
					}catch(Exception e){e.printStackTrace();}
				}
				else{
					System.out.println("risposta ! 200 ok");
				}
			}catch(Exception e){e.printStackTrace();}
			listaallfile.add(risposta);
		}
		return listaallfile;
	}

	private void leggiJson(List<String> daleggere,List<List<String>> lista,int dim){
		for(int j=0;j<dim;j++){
			List<String> app= new ArrayList<String>();
			String listadaleggere=daleggere.get(j);
			String[] parts = listadaleggere.split("\"");
			for(int i=1;i<parts.length;i=i+2){
				System.out.println(parts[i]+" ");
				app.add(parts[i]);
			}
			lista.add(app);
		}
	}

	private int max(List<List<String>> l){
		int index=0;
		for(int j=0;j<l.size();j++){
			if(l.get(j).size()>l.get(0).size())
				index=j;
		}
		//System.out.println("il max � "+index);
		return index;
	}

	private void deploy(List<String> IP, String file) throws IOException {
		for(int i=1;i<IP.size();i++){
			try{
//				String query = "scp -3 -i /Volumes/Transcend/Specs/certificati/rktest.pem root@"+IP.get(0)+":/srv/www/htdocs/manage_app/uploads/"+file+" root@"+IP.get(i)+":/srv/www/htdocs/manage_app/uploads/"+file;
				String query = "scp -3 -o StrictHostKeyChecking=no -i "+this.getClass().getClassLoader().getResource("rktest.pem").getPath()+" root@"+haProxyIp+":/srv/www/htdocs/manage_app/uploads/"+file+" root@"+IP.get(i)+":/srv/www/htdocs/manage_app/uploads/"+file;
				
				System.out.println("deploy - query scp: "+query);
				
				Process proc = Runtime.getRuntime().exec(query);
				proc.waitFor();

//				BufferedReader stdInput = new BufferedReader(new 
//						InputStreamReader(proc.getInputStream()));
//
//				BufferedReader stdError = new BufferedReader(new 
//						InputStreamReader(proc.getErrorStream()));
//
//				// read the output from the command
//				System.out.println("Here is the standard output of the command:\n");
//				String s = null;
//				while ((s = stdInput.readLine()) != null) {
//					System.out.println(s);
//				}
//
//				// read any errors from the attempted command
//				System.out.println("Here is the standard error of the command (if any):\n");
//				while ((s = stdError.readLine()) != null) {
//					System.out.println(s);
//				}

				System.out.println(query);
			} catch(Exception e){
				System.out.println(e);
			}
		}
	}

	private void stampa(List<String> a){
		for(int i=0;i<a.size();i++){
			System.out.println(a.get(i)+" ");
		}
	}
	private void stampa2(List<List<String>> a){
		for(int i=0;i<a.size();i++){
			for(int j=0;j<a.get(i).size();j++)
				System.out.println(a.get(i).get(j));
		}
	}

}
