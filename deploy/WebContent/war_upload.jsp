<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
           
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Manage Your Container</title>

<link href="bootstrap/css/bootstrap.min.css"  type="text/css" rel="stylesheet">
<style type="text/css">
	body {
		padding-top: 60px;
		padding-bottom: 40px;
	}
	
	.sidebar-nav {
		padding: 9px 0;
	}
	a.disabled {
   		pointer-events: none;
   		cursor: default;
	}
	.glyphicon-refresh-animate {
	margin-right: 15px;
    -animation: spin .7s infinite linear;
    -webkit-animation: spin2 .7s infinite linear;
}

@-webkit-keyframes spin2 {
    from { -webkit-transform: rotate(0deg);}
    to { -webkit-transform: rotate(360deg);}
}

@keyframes spin {
    from { transform: scale(1) rotate(0deg);}
    to { transform: scale(1) rotate(360deg);}
}
</style>

<script src="bootstrap/js/jquery.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="js/javascript.js"></script>
<script>
var verifica=false,dim=0;

function controlla(nomefile,id){
	var url = 'upload.do?controlla='+nomefile;
	$.ajax({
	   type: 'GET',
		url: url,
		async: true,
		success: function(data){
			ris=(data.split('�')[2]);
			$('#stato'+id)[0].innerHTML=ris.split(";")[0];
			$('#data'+id)[0].innerHTML=ris.split(";")[1];
			set(id);
			setError(id);
		}
	});
}

function cancella(nomefile,id,size){
	if(!verifica){
	dim=size;
	verifica=true;
	}
	del(nomefile,id,size);
	dim=dim-1;
	if(dim==0){
		$("#table")[0].innerHTML='<div class="panel-body">There are no file uploaded!</div>';
	}
	else $("#"+id)[0].innerHTML="";
	$('#error').css('display','none');
}

function loading(){
	$('#loading_modal').modal('show');
}

$( document ).ready(function() {
	$('#loading_modal').modal('hide');
	$('#ModalFormat').modal('hide');
	var errore='<%= request.getParameter("ERROR") %>';
	if(errore=='File not valid') $('#ModalFormat').modal('show'); 

	var dim = ${listafile.size()};
	for(i=0;i<dim;i++){
		set(i);
		setError(i);
	}
});

function set(id){
	if($('#stato'+id)[0].innerHTML=='running and sync' || $('#stato'+id)[0].innerHTML=='running and not sync') {
		document.getElementById("start"+id).className="disabled";
		document.getElementById("delete"+id).className="disabled";
		document.getElementById("stop"+id).className="";
	}
	else if($('#stato'+id)[0].innerHTML=='deployed and sync' || $('#stato'+id)[0].innerHTML=='deployed and not sync'){
		document.getElementById("start"+id).className="";
		document.getElementById("delete"+id).className="";
		document.getElementById("stop"+id).className="disabled";
	}
}

function setError(id){
	if($('#stato'+id)[0].innerHTML=='running and not sync' || $('#stato'+id)[0].innerHTML=='deployed and not sync') {
		$('#errore'+id).css('display','block');
	}
	else {
		$('#errore'+id).css('display','none');
	}
}


</script>

</head>

<body>

<jsp:include page="./navigator.jsp">
<jsp:param  name="_upload" value="class='active'"/>
</jsp:include>

<!-- <ul class="pager wizard">
					<li class="previous"><a href="./upload.do">Previous</a></li>
					<li class="next disabled"><a href="#">Next</a></li>
				</ul> -->

<div class="panel panel-default">
		
		<div class="panel-heading">
			<h2>Manage Your Container <span style="float:right;">Sla ID:${idsla}</span><input type="hidden" value="�${singolostato}�" /></h2>
		</div>
		
		<div class="panel-body ">

 <!-- ****************************************************************************************** -->
<div class="panel panel-default">	
	<div class="panel-body">
	<form action="FileUpload" method="post" enctype="multipart/form-data"> 
	<div class="input-group">
      <input type="file" class="form-control" placeholder="Upload file" name="fileName" accept=".war,.zip,.tar.gz">
      <input type="hidden" name="idsla" id="idsla1" value="${idsla}"/>
      <span class="input-group-btn">
        <button class="btn btn-primary" type="submit" onclick="javascript:loading();">Upload</button>
      </span>
    </div>
	</form>
	</div>
</div>			

<div class="well">
This list shows all web apps loaded and their state. <br/>
</div>

<div class="panel panel-default" id='table'>	
	<div class="panel-body">
	<c:choose>
		<c:when test="${listafile.size()!=0}">		
		<table class="table">
			<thead>
				<tr>
				<th>File List</th>
				<th>Status</th>
				<th>last check</th>
				<th></th>
				</tr>
			</thead>		
			<!--  <tbody id="corpo">
			</tbody> -->
			<tbody>
					<c:forEach var="file" items="${listafile}" varStatus="i">
						<tr id="${i.index}">
							<td width="20%"><span class="glyphicon glyphicon-file"></span><span class="file" id="file${i.index}">${file}</span></td>
							<td width="20%"><span class="" id="stato${i.index}">${stato[i.index]}</span></td>
							<td width="20%"><span class="" id="data${i.index}">${tempo[i.index]}</span></td>
							<td>
								<div class="btn-group">
								  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								   Force Action <span class="caret"></span>
								   </button>
								  <ul class="dropdown-menu">
								    <li><a href="#" onclick="javascript:controlla('${file}',${i.index})">Force Update</a></li>
								    <li role="separator" class="divider"></li>
								    <li><a href="#" id="sync${i.index}" onclick="javascript:sync('${file}',${i.index})">Force Sync</a></li>
								  </ul>
								</div>
							</td>
							<td>
								<div class="btn-group">
								  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								   Managed App <span class="caret"></span>
								   </button>
								  <ul class="dropdown-menu">
								    <li><a href="#" id="delete${i.index}" onclick="javascript:cancella('${file}',${i.index},${listafile.size()});">Delete</a></li>
								    <li role="separator" class="divider"></li>
								    <li><a href="#" id="start${i.index}" onclick="javascript:start('${file}',${i.index});">Start</a></li>
									<li role="separator" class="divider"></li>
								    <li><a href="#" id="stop${i.index}" onclick="javascript:stop('${file}',${i.index});">Stop</a></li>
								  </ul>
								</div>
							</td>
							<td>
								<!-- Button trigger modal -->
								<button id="errore${i.index}" type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal" style="display:none;">
								  Warning
								</button>
								<!-- Modal -->
								<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								  <div class="modal-dialog" role="document">
								    <div class="modal-content">
								      <div class="modal-header label-warning">
								        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								        <h4 class="modal-title" id="myModalLabel" style="color:white;">Warning:</h4>
								      </div>
								      <div class="modal-body">
								      <p>Web servers not sync </p>
								      </div>
								      <div class="modal-footer">
								        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								      </div>
								    </div>
								  </div>
								</div>
							</td>
						</tr>
					</c:forEach>
			</tbody>
		</table>
		</c:when>
			
		<c:otherwise>
		There are no file uploaded!
		</c:otherwise>
			
	</c:choose>
	</div>
</div>	

								<!-- Modal Info Loading-->
								<div class="modal fade" id="loading_modal" tabindex="-1" role="dialog" aria-labelledby="loading_modalLabel">
								  <div class="modal-dialog" role="document">
								    <div class="modal-content">
								      <div class="modal-header label-warning">
								        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								        <h4 class="modal-title" id="loading_modalLabel" style="color:white;">Loading</h4>
								      </div>
								      <div class="modal-body">
								      	<span><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>Loading...</span>
								      </div>
								      <div class="modal-footer">
								      </div>
								    </div>
								  </div>
								</div>
								
								<!-- Modal Error type of file-->
								<div class="modal fade" id="ModalFormat" tabindex="-1" role="dialog" aria-labelledby="ModalFormatError">
								  <div class="modal-dialog" role="document">
								    <div class="modal-content">
								      <div class="modal-header label-danger">
								        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								        <h4 class="modal-title" id="ModalFormatError" style="color:white;">Error:</h4>
								      </div>
								      <div class="modal-body">
								      <p>Format file is not valid</p>
								      </div>
								      <div class="modal-footer">
								        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="window.location.replace('./war_upload.do?idsla=${idsla}');">Close</button>
								      </div>
								    </div>
								  </div>
								</div>


 
<!-- ****************************************************************************************** -->
</div> <!--  end of main panel body -->
</div> <!--  end of main panel -->
<jsp:include page="./footer.jsp"/>

</body>
</html>