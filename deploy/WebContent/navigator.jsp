<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<div id="_navigator">
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"> <img alt="Brand" src="img/specs-logo-32.png"></a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li ${param._home}><a href="./">Home</a></li>
					<li ${param._rank}><a href="">CSP Ranking</a></li>
					<li ${param._neg}><a href="">Negotiate SLA</a></li>
					<li ${param._sign}><a href="">Sign SLA</a></li>
					<li ${param._impl}><a href="">Implement SLA</a></li>
					<li ${param._obs}><a href="">Observe SLA</a></li>
					<!-- inizio modifica -->
					<li ${param._upload}><a href="./upload.do">Manage Container</a></li>
					<!-- fine modifica -->
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>
</div>

