<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Web Deploy Home</title>

<link href="bootstrap/css/bootstrap.min.css"  type="text/css" rel="stylesheet">
<style type="text/css">
	body {
		padding-top: 60px;
		padding-bottom: 40px;
	}
	
	.sidebar-nav {
		padding: 9px 0;
	}
</style>

</head>
<!-- ********************************-->

<body>

<jsp:include page="./navigator.jsp">
<jsp:param  name="_home" value="class='active'"/>
</jsp:include>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1>Deploy App</h1>
        <p>Welcome to interface for the manage of your web container. <br/>
         <p><a class="btn btn-primary btn-large" href="./upload.do" target="_self">Start Manage &raquo;</a></p>
      </div>
    </div>


<jsp:include page="./footer.jsp"/>

</body>
</html>